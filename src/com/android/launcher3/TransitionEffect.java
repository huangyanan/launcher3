package com.android.launcher3;

import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.Region;
import android.view.View;


public class TransitionEffect {

    private PagedView mPagedView;

    private final Camera mCamera = new Camera();
    private final Matrix mMatrix = new Matrix();
    private final Rect mRect = new Rect();

    private int mCellCountX;
    private int mCellCountY;
    private int mCellHeight;
    private int mCellWidth;
    private int mPageCellCount;
    private float pageWidth;

    public TransitionEffect(PagedView pagedView) {
        mPagedView = pagedView;
        init();
    }

    private void init() {
        if (mPagedView == null)
            return;

        int pageCount = mPagedView.getPageCount();
        CellLayout cell = null;
        for (int index = 0; index < pageCount; index++) {
            View child = mPagedView.getPageAt(index);
            if (child != null) {
                cell = (CellLayout) child;
                break;
            }
        }
        if (cell == null) {
            return;
        }

        pageWidth = cell.getWidth();
        mCellCountX = cell.getCountX();
        mCellCountY = cell.getCountY();
        mCellWidth = cell.getCellWidth();
        mCellHeight = cell.getCellHeight();
        mPageCellCount = (mCellCountX * mCellCountY);
    }

    public void screenScrolled(Canvas canvas, int page, float offsetX, int transitiontype) {
        init();
        if (transitiontype == 0) {
            screenScrolledA(canvas, page, offsetX);
        } else {
            screenScrolledB(canvas, page, offsetX);
        }
    }

    private void screenScrolledA(Canvas canvas, int page, float offsetX) {

        CellLayout localCellLayout = (CellLayout) mPagedView.getChildAt(page);
        float f1 = Math.max(-1.0F, Math.min(1.0F, offsetX));
        float f2 = Math.round(localCellLayout.getWidth() * offsetX);
        offsetX = 1.0F - Math.abs(offsetX);
        offsetX = (float) Math.pow(offsetX, 2.0F + offsetX);
        if (offsetX > 0.001D)
            localCellLayout.getShortcutsAndWidgets().setAlpha(offsetX);
        int index = 0;
        while (index < mPageCellCount) {
            canvas.save();
            int j = index % mCellCountX;
            int i = index / mCellCountX;
            View localView = localCellLayout.getChildAt(j, i);
            index += 1;
            if (localView != null) {
                
                
                CellLayout.LayoutParams localLayoutParams = (CellLayout.LayoutParams) localView.getLayoutParams();
                j = localView.getLeft() + (j - localLayoutParams.cellX) * mCellWidth;
                i = localView.getTop() + (i - localLayoutParams.cellY) * mCellHeight;
                mMatrix.setRotate(-360.0F * f1, localCellLayout.getLeft() + j + mCellWidth / 2, localCellLayout.getTop() + i + mCellHeight / 2);
                canvas.concat(mMatrix);
                mRect.set(localCellLayout.getLeft() + j, localCellLayout.getTop() + i, localCellLayout.getLeft() + j + mCellWidth, localCellLayout.getTop() + i + mCellHeight);
                canvas.clipRect(mRect, Region.Op.INTERSECT);
                mPagedView.drawChildWrap(canvas, localCellLayout, mPagedView.getDrawingTime());
                canvas.restore();
            }
        }

    }

    private void screenScrolledB(Canvas canvas, int page, float offsetX) {
        CellLayout cellLayout = (CellLayout) mPagedView.getChildAt(page);
        float f = Math.max(-1.0F, Math.min(1.0F, offsetX));
        int i = Math.round((pageWidth + mPagedView.getPaddingLeft()) * offsetX);
        int index = 0;
        while (index < mPageCellCount) {
            int k = index % mCellCountX;
            int j = index / mCellCountX;
            View view = cellLayout.getChildAt(k, j);
            index++;
            if (view != null) {
                CellLayout.LayoutParams localLayoutParams = (CellLayout.LayoutParams) view.getLayoutParams();
                k = view.getLeft() + (k - localLayoutParams.cellX) * mCellWidth;
                j = view.getTop() + (j - localLayoutParams.cellY) * mCellHeight;
                mCamera.save();
                mCamera.setLocation(0.0F, 0.0F, mCellWidth / 2);
                mCamera.rotateY(-180.0F * f);
                mCamera.getMatrix(mMatrix);
                mMatrix.preTranslate(-cellLayout.getLeft() - k - mCellWidth / 2, -cellLayout.getTop() - j);
                mMatrix.postTranslate(cellLayout.getLeft() + k + mCellWidth / 2, cellLayout.getTop() + j);
                mCamera.restore();
                canvas.save();
                if ((offsetX >= -0.5F) && (offsetX <= 0.5F)) {
                    canvas.translate(i, 0.0F);
                    if (view.getVisibility() != View.VISIBLE)
                        view.setVisibility(View.VISIBLE);
                } else {
                    canvas.translate(pageWidth * -10.0F, 0.0F);
                }

                canvas.concat(mMatrix);
                mRect.set(cellLayout.getLeft() + k, cellLayout.getTop() + j, cellLayout.getLeft() + k + mCellWidth, cellLayout.getTop() + j + mCellHeight);
                canvas.clipRect(mRect, Region.Op.INTERSECT);
                mPagedView.drawChildWrap(canvas, cellLayout, mPagedView.getDrawingTime());
                canvas.restore();
            }
        }
    }


}